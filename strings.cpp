#include <stdio.h>
//#include <string.h>		//подключаем, если нужно подсмотреть определение стандартных функций обработки строк

char* s_gets(char* str, unsigned buf_size);		//безопасная реализация gets на базе fgets
char* s_gets_c(char* str, unsigned buf_size);		//безопасная реализация gets на базе getchar
unsigned str_len(char* str);				//определение длины строки
char* str_cpy(char* dst, char* src);		//копированис строки
char* str_cat(char* str1, char* str2);		//сцепление строк
int str_cmp(char* str1, char* str2);		//сравнение строк
char* str_str(char* str, char* sub);		//поиск подстроки в строке

char* remove_char(char* str, unsigned pos);	//удаление символа в заданной позиции со сдвигом
char* remove_duplicates(char* str);			//удаление дубликатов символов

unsigned sum_of_digits(char* str);			//сумма цифр в строке
unsigned sum_of_decimals(char* str);		//сумма чисел в строке

void swap(char* pa, char* pb);				//обмен значениями между переменными

char* reverse_string(char* str);			//перестановка всех символов строки в обратном порядке
char* reverse_substring(char* str, int left, int right);	//перестановка символов в заданном диапазане индексов 

unsigned char_in_string(char symbol, char* str);	//определяем вхождение символа в строку
unsigned words_count(char* str);		//подсчёт числа слов в строке
unsigned longest_word(char* str);		//определение длины самого длинного слова в строке
unsigned shortest_word(char* str);		//определение длины самого короткого слова в строке
void print_words(char* str);			//распечатка слов строки по отдельности
char* reverse_words(char* str);			//перестановка символов внутри каждого слова

int main()
{
	const unsigned buf_size = 50;
	char str1[buf_size];
	char str2[buf_size];

	s_gets_c(str1, buf_size);
	//s_gets(str2, buf_size);

	//printf("%u", sum_of_digits(str1));
	//printf("%u", sum_of_decimals(str1));

	//puts(remove_duplicates(str1));
	//puts(reverse_string(str1));
	
	puts(str1);
	puts(reverse_words(str1));

	//print_words(str1);
	//printf("%u", shortest_word(str1));

	getchar();
    return 0;
}

char* s_gets(char* str, unsigned buf_size)
{
	/*
	Безопасная (с точки зрения риска переполненя буфера)
	реализация чтения строки с консоли
	Является обёрткой вызова функции fgets
	*/

	unsigned i = 0;
	//если вызов fgets завершился возвратом NULL
	if (!fgets(str, buf_size, stdin))
		//тоже вернём NULL - признак ошибки ввода
		return NULL;

	//fgets не вырезает символ перевода строки
	//это удобно для файлов, а при вводе с клавиатуры
	//придётся сделать это самим
	do
		if (str[i] == '\n')
			break;
	while (str[++i]);
		
	//символ конца строки впишем на ту позицию,
	//на которой остановился цикл
	//это будет либо при чтении '\0', либо '\n'
	str[i] = '\0';

	//часто дополнительно обрабатывают ситуацию,
	//когда fgets завершился, поскольку исчерпан буфер
	//при этом вычитывают всё, что осталось в потоке ввода
	//обратите внимание, что тело цикла пусто
	//вся полезная работа делается в ходе проверки условия
	//за счёт вызова getchar
	while (getchar() != '\n');

	return str;
}

char* s_gets_c(char* str, unsigned buf_size)
{
	/*
	Безопасная (с точки зрения риска переполненя буфера)
	реализация чтения строки с консоли
	Основана на посимвольном чтении потока ввода (getchar)
	*/

	unsigned i = 0;
	int ch;

	//в данном случае из потока ввода забираем всё,
	// что есть до символа '\n' (когда будет нажат ввод)
	while ((ch = getchar()) != '\n')
		//но записываем только пока есть место в строке
		if (i < (buf_size - 1))
			str[i++] = ch;
	//в конце не забываем поставить символ конца строки
	str[i] = '\0';
		
	return str;
}

unsigned str_len(char* str)
{
	/*
	Функция определения длины строки
	считается, что на вход подаётся правильно сформированная строка
	(завершающаяся '\0')
	контроллировать размер буфера не требуется,
	т.к. мы гарантированно не будем менять стороку
	*/

	unsigned i = 0;
	//обратите внимание, что тело цикла пусто
	//вся полезная работа делается в ходе проверки условия
	//за счёт инкремента
	while (str[i++]);

	//не забываем про коррекцию
	return (i - 1);
}

char* str_cpy(char* dst, char* src)
{
	/*
	Функция копирования строк
	содержимое строки src (от слова source)
	пишется поверх строки dst (от слова destination)
	Реализация небезопасна!
	мы никак не проверяем,
	достаточно ли памяти выделено при объявлении dst
	Считается, что такого рода проверку программист
	должен сделать самостоятельно перед вызовом функции
	*/

	unsigned i = 0;
	do
		dst[i] = src[i];
	while (src[i++]);

	//как правило все функции обработки строк возвращают
	//указатель на char, который ссылается на строку-результат
	//это позволяет в месте вызова тут же воспользоваться результатом
	return dst;
}

char* str_cat(char* str1, char* str2)
{
	/*
	Функция конкатенации (сцепления) строк
	содержимое строки str2 дописывается 
	после str1
	Реализация небезопасна!
	мы никак не проверяем,
	достаточно ли памяти выделено при объявлении str1
	Считается, что такого рода проверку программист
	должен сделать самостоятельно перед вызовом функции
	*/
	
	/*
	можно организовать копирование посимвольно
	важно только правильно посчитать,
	с какой позиции в первой строке
	нужно начинать копировать
	символы второй строки
	мы должны стереть символ '\0' в первой строке,
	записав на его место стартовый символ второй строки

	unsigned str1_len = str_len(str1);
	unsigned i = 0;
	do
		str1[i + str1_len] = str2[i];
	while (str2[i++]);
	
	return str1;*/

	//можно свести реализацию сцепления к копированию
	//копируем вторую строку по адресу,
	//соответствующему символу конца строки в str1
	return str_cpy(str1 + str_len(str1), str2);
}

int str_cmp(char* str1, char* str2)
{
	/*
	Функция для сравнения строк
	строки сравниваются посимвольно
	если они полностью совпадают - возвращается 0
	если в паре несовпадающих символов больше код символа,
	стоящего в первой строке - возвращаеся положительное число,
	в противном случае - отрицательное
	*/
	unsigned i = 0;
	int delta;


	//используем разность кодов символов
	//как основной критерий продолжения цикла
	do
	{
		delta = str1[i] - str2[i];
	} while (!delta && str1[i++]);
	//кроме того при завершении первой строки также выходим
	//если же вторая строка завершиться раньше,
	//то сработает выход по delta
	
	return (delta);
}

char* str_str(char* str, char* sub)
{
	/*
	Функция поиска подстроки (sub) в строке (str)
	возвращает указатель на символ,
	с которого начинается первое вхождение подстроки
	если вхождения нет, возвращаем NULL
	*/

	unsigned i = 0, j, flag;

	do
	{
		j = 0;
		flag = 1;
		do 
			//если нашли несовпадающие символы
			if (str[i + j] != sub[j])
			{
				//сбросили флаг и вышли
				flag = 0;
				break;
			}
		//обращаем внимание, что увеличение j здесь выполняется
		//ПЕРЕД проверной условия продолдения цикла
		while (sub[++j]);
		//если в процессе сравнения флаг не был сброшен,
		//то мы нашли искомую позицию
		if (flag) return (str+i);
	} while (str[i++]);

	return NULL;
}

char* remove_char(char* str, unsigned pos)
{
	/*
	Функция удаления символа с заданным индексом
	*/

	//реализцетя по сути сдвиг вправо,
	//начиная с заданной позиции
	do
		str[pos] = str[pos + 1];
	while (str[pos++]);
	//только цикл организоваy на основе проверки на '\0'

	//и возвращаем char*,
	//как это обычно делается в функциях обработки строк
	return str;
}

char* remove_duplicates(char* str)
{
	/*
	Функция удаления повторных вхождений символов в строку
	Простейшие реализации обычно основаны
	на удалении найденных повторных символов,
	что приводит к многократным сдвигам массива
	В данной реализации повторные символы не удаляются,
	а просто пропускаются.
	Зато впервые встреченные символы копируются в начало строки.
	Т.е. в начале строки начинает формироваться ответ,
	по завершении обработки всей строки после него надо будет
	только дописать '\0'.
	Тем самым мы не только избегаем сдвигов,
	но и упрощаем процедуру проверки на дубликаты.
	Уникальных символов заведомо не может быть больше,
	чем всего символов в кодовой таблице (255).
	А строка при этом может быть гораздо длиннее.
	Важно заметить, что в общем случае счётчик символов i
	обгоняет счётчик уникальных символов count,
	но между ними располагаются дублирующиеся элементы,
	поэтому на их место можно смело записывать новые обнаруженные
	уникальные элементы
	*/

	unsigned count = 1, i = 0, j, flag;
	
	while (str[i])
	{
		flag = 1;
		//проверка на унивальность
		//поиск среди первых (count-1) элементов
		for (j=0; j < count; j++)
			if (str[j] == str[i])
			{
				flag = 0;
				break;
			}
		//если флаг не сброшен
		if (flag)
			//добавляем символ к числу уникальных			
			str[count++] = str[i];
		i++;
	}
	str[count] = '\0';

	return str;

}

unsigned sum_of_digits(char* str)
{
	/*
	Функция вычисления суммы ЦИФР,
	входящих в состав строки
	*/
	unsigned i = 0, sum = 0;

	do
		//если символ является цифрой
		if (str[i] >= '0' && str[i] <= '9')
			//добавляем значение цифры к сумме
			//поскольку в кодовой таблице цифры
			//записаны подряд, числовое значение
			//можно получить вычислив разность
			//кода текущего символа с кодом символа '0'
			sum += str[i] - '0';
	while (str[i++]);

	return sum;
}

unsigned sum_of_decimals(char* str)
{
	/*
	Функция вычисления суммы ЧИСЕЛ,
	входящих в состав строки
	*/

	unsigned i = 0, sum = 0, decimal = 0;

	do
	{
		//пока встречаем в подряд идущие цифры
		while (str[i] >= '0' && str[i] <= '9')
		{
			//формируем число
			decimal = decimal * 10 + (str[i] - '0');
			i++;
		}
		//если символ не цифра
		//добавляем подсчитанное число к сумме
		sum += decimal;
		//и готовимся читать следующее число
		decimal = 0;
	}
	while (str[i++]);

	return sum;
}

void swap(char* pa, char* pb)
{
	char temp = *pa;
	*pa = *pb;
	*pb = temp;
}

char* reverse_string(char* str)
{
	/*
	Переворот строки
	*/

	//сводится к перевороту подстроки,
	//но захватывая все элементы строки
	return reverse_substring(str, 0, str_len(str)-1);
}

char* reverse_substring(char* str, int left, int right)
{
	/*
	Переворот подстроки
	В начале возможно добавление проверки
	на корректность исходных данных
	(left и right)
	*/

	while (left < right)
		swap(str + left++, str + right--);
	
	return str;
}

unsigned char_in_string(char symbol, char* str)
{
	/*
	Функция проверяет, встречается ли символ в строке
	Если встречается - возвращаем 1, если нет - 0
	Важно!!! Функция написана так,
	чтобы анализировать и символ '\0',
	который неявно существует в любой строке в C
	*/
	unsigned i = 0;
	
	do
		if (str[i] == symbol)
			return 1;
	while (str[i++]);
	//при такой организации цикла проверяется и символ '\0'

	return 0;
}

unsigned words_count(char* str)
{
	/*
	Функция расчёта числа слов в строке
	Слова отделяются символами-разделителями,
	которые для удобства сгруппированы в отдельную
	строку separator. В некоторых случаях удобно
	передавать такую строку как второй аргумент функции.
	При написании подобных функций важно помнить,
	что она должна быть устойчива к появлению разделителей
	в начале строки, нескольких разделителей вподряд,
	корректно обрабатывать слово, за которым следует
	конец строки
	*/

	unsigned inWord = 0, i = 0, count = 0;
	char separator[] = " \t,.!?:;";

	do
	{
		if (char_in_string(str[i], separator))
		{
			if (inWord)
				count++;
			inWord = 0;
		}
		else
			inWord = 1;
	} while (str[i++]);
	//при этом символ '\0' тоже будет обработан

	return count;
}

unsigned longest_word(char* str)
{
	/*
	Функция нахождения длины самого длинного слова
	Общие принципы организации - аналогичны подсчёту
	числа слов в строке (см. функцию words_count).
	Проходя по строке,
	будем считать длину каждого очередного слова.
	Если она окажется больше текущей оценки
	максимальной длины - обновляем эту оценку.
	Изначально оценку полагаем равной 0.
	*/

	unsigned inWord = 0, i = 0, length = 0, maxLength = 0;
	char separator[] = " \t,.!?:;";

	do
	{
		if (char_in_string(str[i], separator))
		{
			if (inWord)
				if (length > maxLength)
					maxLength = length;
			inWord = 0;
			length = 0;
		}
		else
		{
			inWord = 1;
			length++;
		}
	} while (str[i++]);
	//при этом символ '\0' тоже будет обработан

	return maxLength;
}

unsigned shortest_word(char* str)
{
	/*
	Функция нахождения длины самого короткого слова
	Общие принципы организации - аналогичны подсчёту
	числа слов в строке (см. функцию words_count).
	Проходя по строке,
	будем считать длину каждого очередного слова.
	Если она окажется меньше текущей оценки
	минимальной длины - обновляем эту оценку	
	Изначально оценку полагаем равной 0.
	Если при сравнении этих величин понимаем,
	что оценка до сих пор нулевая
	(т.е. прочитанное слово является первым),
	то его длину принимаем за оценку минимальной длины.
	*/

	unsigned inWord = 0, i = 0, length = 0, minLength = 0;
	char separator[] = " \t,.!?:;";

	do
	{
		if (char_in_string(str[i], separator))
		{
			if (inWord)
				if ((length < minLength) || !minLength)
					minLength = length;
			inWord = 0;
			length = 0;
		}
		else
		{
			inWord = 1;
			length++;
		}
	} while (str[i++]);
	//при этом символ '\0' тоже будет обработан

	return minLength;
}

void print_words(char* str)
{
	/*
	Функция печатает слова, образующие строку,
	в отдельных строках потока вывода
	(отделяя их символом '\n').
	Общие принципы организации - аналогичны подсчёту
	числа слов в строке (см. функцию words_count).
	*/

	unsigned inWord = 0, i = 0;
	char separator[] = " \t,.!?:;";

	do
	{
		if (char_in_string(str[i], separator))
		{
			if (inWord)
				putchar('\n');
			inWord = 0;
		}
		else
		{
			inWord = 1;
			putchar(str[i]);
		}
	} while (str[i++]);
	//при этом символ '\0' тоже будет обработан
}

char* reverse_words(char* str)
{
	/*
	Функция обнаруживает слова в строке
	и переставляет все символы в каждом
	из них в обратном порядке.
	Общие принципы организации - аналогичны подсчёту
	числа слов в строке (см. функцию words_count).
	Важно, что в данном случае фиксируются индексы
	начала слова (left) и конца слова (right).
	Они передаются как аргументы ранее написанной
	функции reverse_substring.
	*/

	unsigned inWord = 0, i = 0, left=0, right=0;
	char separator[] = " \t,.!?:;";

	do
	{
		if (char_in_string(str[i], separator))
		{
			if (inWord)
			{
				right = i - 1;
				reverse_substring(str, left, right);
			}
			inWord = 0;
		}
		else
		{
			if (!inWord)
			{
				left = i;
				inWord = 1;
			}
			
		}
	} while (str[i++]);
	//при этом символ '\0' тоже будет обработан

	return str;
}